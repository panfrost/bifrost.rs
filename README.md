# bifrost.rs

Pie-in-the-sky Bifrost compiler.

## Instruction selection

## Scheduling

Bifrost nests structure. Within each basic block exist a series of _clauses_,
within each clause exist up to 8 _tuples_, and within each tuple exist up to 2
_instructions_.

### Opcodes imply units

Most instructions can only run on FMA or ADD but not both. A few can run on
either, of those there may be restrictions on modifiers in one form due to
space constraints. For a given instruction, regardless of context, one can use
the opcode (and perhaps the modifiers) to determine if it can be scheduled to
FMA, ADD, or both. See [unit reference](Units.md) for a listing.

### Scheduling implies slot assignment

Before a tuple, the register file is accessed according to the tuple's register
block, encoding the registers read by the tuple and the registers written by
the preceding tuple (wraparound to last tuple for first in clause)

A register block contains 2 read slots, 1 read/write (FMA) slot, and 1 write
slot (FMA or ADD), such that:

* Selected registers are disjoint (except for masked 16-bit writes and the
  write for the last tuple specified on the first)

* First two sources of FMA instruction must not read from the read/write slot.

* Last tuple can only write a single register.

Not subject to these constraints are passthrough sources. An instruction can
access the destinations from the previous tuple, and an ADD instruction can
access the destination of its corresponding FMA. Hence locality of access is
crucial.

However, not all instructions can read all sources. In particular table-like
instructions (including, strangely, `+ATEST`) cannot read their tuple's FMA
result. These restrictions are encoded in the `mask` field of the `src`
elements of `ISA.xml` in Mesa.

Due to these limitations, slots must be assigned during scheduling. However, it
is *not* necessary to assign registers during scheduling; register allocation
can still proceed after scheduling. Since writes are for the preceding tuple,
the disjoint constraint amounts to mandating the use of passthrough sources; it
does not affect register allocation.

### Scheduling implies FAU assignment

Constants are accessed from the Fast Access Uniform (FAU-RAM) table. A single
tuple may specify only a single FAU index, so a tuple may access only one of:

* A 64-bit uniform, **OR**
* A 64-bit constant

In particular, across the two paired instructions, only 2 (32-bit) constants
are allowed. This hazard can be worked around by introducing moves in the
immediately preceding tuple and passing through, which does not affect slot
assignment. This strategy does not work for the first tuple of a given clause.

Furthermore, a clause must not exceed eight 128-bits words. This is enforced by the encoding, implying the total count of tuples and instructions must not exceed $13$. Recall a tuple can read at most one 64-bit constant and there is at least one but no more than 8 tuples per clause. Finally note that the clause packing implicitly encodes a single 64-bit constant when there are 3, 5, 6, or 8 tuples in the clause.

Let $C$ denote the count of 64-bit constants and $T$ the tuple count. Let $\mathcal{P}$ denote the set of $(c, t)$ of possible constant word positions for constant index $c$ and total tuple count $t$. We have (proof: `models/pos.py`): 

> If $1 \leq T \leq 8, 0 \leq C \leq T, C + T \leq 13$, then for each $i \in \mathbb{N}$, if $i$ is even _exclusive-xor_ $T \in \{ 3, 5, 6, 8 \}$, then $(i, t) \in \mathcal{P}$.

Plainly, the constant words in the clause may be packed if $C + T \leq 13$.

Scheduling can be helped by optimizing out uniform-on-constant calculations.

### Staging register access is delayed

Message-passing instructions like `LOAD.i32` do not perform any work
themselves. Instead, they pass messages to other parts of the chip (suggestive
name, eh?) Their results are thus not immediately visible in a clause, even if
they execute early in the clause. Instead a clause containing a message-passing
instruction must be assigned a scoreboard index and waited upon by the clause
that consumes the result.

### Some instructions must be paired

Some complex instructions operate as two parts, where the first part executes
in the `FMA` pipe and the second in the `ADD` pipe. In particular this allows
the "combined" instruction write 64-bits instead of the usual 32-bits. Other
instructions can be modified if paired in a tuple.
 
* `*ATOM_C/+ATOM_CX`
* `*ATOM_C1/+ATOM_CX`
* `*ATOM_C_RETURN/+ATOM_CX`
* `*ATOM_C1_RETURN/+ATOM_CX`
* `*ATOM_PRE/+ATOM_CX`
* `*CUBEFACE1/+CUBEFACE2`
* `*IMULD/+IMOV_FMA`
* `*JUMP_EX/+JUMP` or any branch (for far jumps and procedure calls)
* `*SEG_ADD/+SEG_ADD`
* `*SEG_SUB/+SEG_SUB`
* `*SHADDXL/+SHADDXH`
* `*SHADDXL/+load, store, or atomic` (for fused address)
* `*shift/+SHIFT_DOUBLE.i32` (for 64-bit)
* `*VN_ASST1/+VN_ASST2`
* `*DTSEL_IMM/+resource access` (for bindless access)

### Some instructions must be the last in a clause

Atomics, branches, and barriers must be scheduled as the last tuple of
their clause. `BLEND` is included here as blending is a hidden conditional
branch, possibly jumping into a blending shader. Other message-passing
instructions are not subject to this requirement.

* `*ATOM_C`
* `*ATOM_C1`
* `*ATOM_C1_RETURN`
* `*ATOM_C_RETURN`
* `+BARRIER`
* `+BLEND`
* `+BRANCH`
* `+BRANCHC`
* `+BRANCH_DIVERG`
* `+BRANCH_LOWBITS`
* `+BRANCH_NO_DIVERG`
* `+BRANCHZ`
* `+JUMP`
* `+JUMP_EX`

Additionally, `*ATOM_PRE.i32` must execute in the second-to-last tuple of a
clause, where the last clause contains the `*ATOM_C(_RETURN)` instruction.  `

### Message-passing instructions induce clauses

A clause contains at most 1 message-passing instruction. Thus a first guess at
a schedule will assign a clause per message-passing instruction and set
dependencies between clauses according to the dependencies of the
message-passing instructions. 

## Register allocation

## Packing
