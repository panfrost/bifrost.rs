Diagrams/DAG.png: Diagrams/DAG.dot
	dot Diagrams/DAG.dot -Tpng -o Diagrams/DAG.png

Diagrams/DAG-reduced.png: Diagrams/DAG-reduced.dot
	dot Diagrams/DAG-reduced.dot -Tpng -o Diagrams/DAG-reduced.png

Diagrams/DAG-2.png: Diagrams/DAG-2.dot
	dot Diagrams/DAG-2.dot -Tpng -o Diagrams/DAG-2.png

Diagrams/DAG-3.png: Diagrams/DAG-3.dot
	dot Diagrams/DAG-3.dot -Tpng -o Diagrams/DAG-3.png
