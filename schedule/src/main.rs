// Prototype Bifrost scheduler implementing the model described in the accompanying white paper
//
// Copyright (C) 2020 Collabora, Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice (including the next paragraph) shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#[derive(Debug, PartialEq)]
enum Units {
    FMA,
    ADD,
    Both
}

// Define some opcodes. This is opaque to the model, which just gets the associated accessors

#[derive(Debug, PartialEq, Copy, Clone)]
enum Opcode {
    Fadd,
    FMA,
    Jump,
    Load,
    Nop,
    Store,
}

fn units(op : Opcode) -> Units {
    match op {
        Opcode::FMA => Units::FMA,
        Opcode::Load | Opcode::Store | Opcode::Jump => Units::ADD,
        Opcode::Fadd | Opcode::Nop => Units::Both
    }
}

fn last(op : Opcode) -> bool {
    match op {
        Opcode::Jump => true,
        _ => false
    }
}

fn message(op : Opcode) -> bool {
    match op {
        Opcode::Load | Opcode::Store => true,
        _ => false
    }
}

// TODO: add some more ops so these aren't trivial

fn read0(_op : Opcode) -> bool {
    true
}

fn read_t(op : Opcode) -> bool {
    match op {
        Opcode::Jump => false,
        _ => true
    }
}

fn read_t0_t1(_op : Opcode) -> bool {
    true
}

fn staging(ins: &Instruction) -> Vec<Value> {
    let mut vec = Vec::with_capacity(1);

    match ins.op {
        Opcode::Load => vec.push(ins.dst.unwrap()),
        Opcode::Store => vec.push(ins.src[0]),
        _  => {},
    };

    vec
}

#[test]
fn test_opcodes() {
    assert_eq!(units(Opcode::FMA), Units::FMA);
    assert_eq!(units(Opcode::Fadd), Units::Both);
    assert_eq!(units(Opcode::Nop), Units::Both);

    assert_eq!(last(Opcode::Jump), true);
    assert_eq!(last(Opcode::Fadd), false);

    assert_eq!(message(Opcode::Load), true);
    assert_eq!(message(Opcode::Jump), false);
    assert_eq!(message(Opcode::Fadd), false);

    assert_eq!(read0(Opcode::Fadd), true);
    assert_eq!(read_t(Opcode::Fadd), true);
    assert_eq!(read_t0_t1(Opcode::Fadd), true);

    assert_eq!(read0(Opcode::Jump), true);
    assert_eq!(read_t(Opcode::Jump), false);
    assert_eq!(read_t0_t1(Opcode::Jump), true);
}

#[test]
fn test_opcode_assumption() {
    let opcodes : [Opcode; 6] = [
        Opcode::Fadd,
        Opcode::FMA,
        Opcode::Jump,
        Opcode::Load,
        Opcode::Nop,
        Opcode::Store
    ];

    for op in opcodes.iter() {
        if last(*op) || message(*op) {
            assert_eq!(units(*op), Units::ADD)
        }
    }
}

#[test]
fn test_staging() {
    let load = Instruction {
        op: Opcode::Load,
        src: vec![Value::SSA(12)],
        dst: Some(Value::Reg(17)),
        fau: Fau::Constants(0, [0, 0])
    };

    let store = Instruction {
        op: Opcode::Store,
        src: vec![Value::SSA(12)],
        dst: Some(Value::Reg(17)),
        fau: Fau::Constants(0, [0, 0])
    };

    let fma = Instruction {
        op: Opcode::FMA,
        src: vec![],
        dst: None,
        fau: Fau::Constants(0, [0, 0])
    };

    assert_eq!(staging(&load), vec![Value::Reg(17)]);
    assert_eq!(staging(&store), vec![Value::SSA(12)]);
    assert_eq!(staging(&fma), vec![]);
}

// Define data structures for the model

#[derive(Debug, PartialEq, Copy, Clone)]
enum Value {
    SSA(u32),
    Reg(u32)
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Fau {
    // Set of strictly increasing constants
    Constants(usize, [u32; 2]),
    Other(u32)
}

struct Instruction {
    op : Opcode,

    // corresponds to use/def, imposing an order and assume an instruction has at most one
    // destination. Assuming only one staging value
    src : Vec<Value>,
    dst : Option<Value>,
    fau: Fau,
}

struct Tuple {
    fma: Instruction,
    add: Instruction
}

type Clause = Vec<Tuple>;

fn tuple_constants(t: &Tuple) -> Vec<u32> {
    use Fau::*;

    match (t.fma.fau.clone(), t.add.fau.clone()) {
        // If there are constants, take the union, possibly removing 0 from FMA
        (Constants(nfma, fma), Constants(nadd, add)) => {
            let mut both = vec![];
            both.extend(fma.iter().take(nfma).filter(|&&x| !(x == 0 && read0(t.fma.op))));
            both.extend(add.iter().take(nadd));
            both.sort();
            both.dedup();
            both
        },

        // No constants but a FAU access
        (Constants(n, _), Other(_)) | (Other(_), Constants(n, _)) => {
            assert_eq!(n, 0);
            vec![]
        },

        // If there are no constants but two FAU accesses
        (Other(fma), Other(add)) => {
            assert_eq!(fma, add);
            vec![]
        }
    }
}

fn nconstants(clause: &Clause) -> usize {
    let mut single = Vec::<u32>::new();
    let mut pairs = Vec::<[u32; 2]>::new();

    for tuple in clause {
        let constants = tuple_constants(tuple);

        if constants.len() > 1 && pairs.iter().all(|v| v != constants.as_slice()) {
            assert_eq!(constants.len(), 2);
            pairs.push([constants[0], constants[1]]);
        } else if constants.len() == 1 && single.iter().all(|&v| v != constants[0]) {
            single.push(constants[0])
        }
    }

    // Deduplicate
    let num_single = single.iter().filter(|s| pairs.iter().all(|pair| !pair.contains(s))).count();
    let single_pairs = (num_single + 1) >> 1;

    return pairs.len() + single_pairs
}

fn make_tuple_fau(fau1: Fau, fau2: Fau) -> Tuple {
    let fma = Instruction {
        op: Opcode::FMA,
        src: vec![],
        dst: None,
        fau: fau1
    };

    let add = Instruction {
        op: Opcode::Fadd,
        src: vec![],
        dst: None,
        fau: fau2
    };

    Tuple { fma, add }
}
 
#[test]
fn test_tuple_constants() {
    let none = Fau::Constants(0, [0, 0]);
    let one = Fau::Constants(1, [7, 0]);
    let two = Fau::Constants(2, [14, 42]);
    let two_over = Fau::Constants(2, [7, 14]);
    let one_zero = Fau::Constants(1, [0, 0]);
    let two_zero = Fau::Constants(2, [0, 7]);
    let other = Fau::Other(0);

    assert_eq!(tuple_constants(&make_tuple_fau(other, none)), vec![]);
    assert_eq!(tuple_constants(&make_tuple_fau(none, other)), vec![]);
    assert_eq!(tuple_constants(&make_tuple_fau(none, none)), vec![]);
    assert_eq!(tuple_constants(&make_tuple_fau(none, one)), vec![7]);
    assert_eq!(tuple_constants(&make_tuple_fau(one, none)), vec![7]);
    assert_eq!(tuple_constants(&make_tuple_fau(none, two)), vec![14, 42]);
    assert_eq!(tuple_constants(&make_tuple_fau(two, none)), vec![14, 42]);
    assert_eq!(tuple_constants(&make_tuple_fau(two_over, two)), vec![7, 14, 42]);
    assert_eq!(tuple_constants(&make_tuple_fau(two_zero, two)), vec![7, 14, 42]);
    assert_eq!(tuple_constants(&make_tuple_fau(none, one_zero)), vec![0]);

    let clause_0 = vec![
        make_tuple_fau(other, none),
    ];

    let clause_1 = vec![
        make_tuple_fau(other, none),
        make_tuple_fau(one, none),
        make_tuple_fau(none, none),
    ];

    let clause_2 = vec![
        make_tuple_fau(other, none),
        make_tuple_fau(two, none),
        make_tuple_fau(none, none),
    ];

    let clause_3 = vec![
        make_tuple_fau(other, none),
        make_tuple_fau(two, none),
        make_tuple_fau(two_over, none),
    ];

    let clause_4 = vec![
        make_tuple_fau(other, none),
        make_tuple_fau(one, none),
        make_tuple_fau(two_over, none),
    ];

    assert_eq!(nconstants(&clause_0), 0); // []
    assert_eq!(nconstants(&clause_1), 1); // [7]
    assert_eq!(nconstants(&clause_2), 1); // [14 42]
    assert_eq!(nconstants(&clause_3), 2); // [14 42] [7 14]
    assert_eq!(nconstants(&clause_4), 1); // [7 14]
}

fn main() {
    println!("Hello, world!");
}
