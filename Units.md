# FMA

Bitwise operations, pre-atomics, CSEL, multiplies, stage-1.

* `ARSHIFT`
* `ARSHIFT_DOUBLE`
* `ATOM_C`
* `ATOM_C1`
* `ATOM_C1_RETURN`
* `ATOM_C_RETURN`
* `ATOM_POST`
* `ATOM_PRE`
* `BITREV`
* `CLZ`
* `CSEL`
* `CUBEFACE1`
* `DTSEL_IMM`
* `FADD_LSCALE`
* `FLSHIFT_DOUBLE`
* `FMA`
* `FMA_RSCALE`
* `FMUL_CSLICE`
* `FMUL_SLICE`
* `FRSHIFT_DOUBLE`
* `IADDC`
* `IDP`
* `IMUL`
* `IMULD`
* `ISUBB`
* `JUMP_EX`
* `LROT_DOUBLE`
* `LSHIFT_AND`
* `LSHIFT_DOUBLE`
* `LSHIFT_OR`
* `LSHIFT_XOR`
* `POPCOUNT`
* `RROT_DOUBLE`
* `RSHIFT_AND`
* `RSHIFT_DOUBLE`
* `RSHIFT_OR`
* `RSHIFT_XOR`
* `SHADDXL`
* `VN_ASST1`

# ADD

Post-atomics, message passing, branching, cross-lane permute, stage-1 ops, conversions, transcendentals, adds.

* `ACMPSTORE`
* `ACMPXCHG`
* `ATEST`
* `ATOM_CX`
* `AXCHG`
* `BARRIER`
* `BLEND`
* `BRANCH`
* `BRANCHC`
* `BRANCHZ`
* `BRANCH_DIVERG`
* `BRANCH_LOWBITS`
* `BRANCH_NO_DIVERG`
* `CLPER`
* `CUBEFACE2`
* `CUBE_SSEL`
* `CUBE_TSEL`
* `DISCARD`
* `DOORBELL`
* `EUREKA`
* `F16_TO_S32`
* `F16_TO_U32`
* `F32_TO_S32`
* `F32_TO_U32`
* `FADD_RSCALE`
* `FATAN_ASSIST`
* `FATAN_TABLE`
* `FCOS_TABLE`
* `FEXP`
* `FEXP_TABLE`
* `FLOGD`
* `FLOG_TABLE`
* `FMAX`
* `FMIN`
* `FPCLASS`
* `FPOW_SC_APPLY`
* `FPOW_SC_DET`
* `FRCBRT_APPROX_A`
* `FRCBRT_APPROX_B`
* `FRCBRT_APPROX_C`
* `FRCP`
* `FRCP_APPROX`
* `FRSQ`
* `FRSQ_APPROX`
* `FSINCOS_OFFSET`
* `FSIN_TABLE`
* `HADD`
* `IABS`
* `IADD`
* `ICMP`
* `ICMPF`
* `ICMPI`
* `ICMPM`
* `ILOGB`
* `IMOV_FMA`
* `ISUB`
* `JUMP`
* `KABOOM`
* `LDEXP`
* `LD_ATTR`
* `LD_ATTR_IMM`
* `LD_ATTR_TEX`
* `LD_CVT`
* `LD_GCLK`
* `LD_TILE`
* `LD_VAR`
* `LD_VAR_FLAT`
* `LD_VAR_FLAT_IMM`
* `LD_VAR_IMM`
* `LD_VAR_SPECIAL`
* `LEA_ATTR`
* `LEA_ATTR_IMM`
* `LEA_ATTR_TEX`
* `LEA_TEX`
* `LEA_TEX_IMM`
* `LOAD`
* `LOGB`
* `MUX`
* `S16_TO_F32`
* `S32_TO_F32`
* `S8_TO_F32`
* `SHADDXH`
* `SHIFT_DOUBLE`
* `STORE`
* `ST_CVT`
* `ST_TILE`
* `SWZ`
* `TEXC`
* `TEXS_2D`
* `TEXS_CUBE`
* `U16_TO_F32`
* `U32_TO_F32`
* `U8_TO_F32`
* `V2F16_TO_V2S16`
* `V2F16_TO_V2U16`
* `V2S16_TO_V2F16`
* `V2S8_TO_V2F16`
* `V2S8_TO_V2S16`
* `V2U16_TO_V2F16`
* `V2U8_TO_V2F16`
* `V2U8_TO_V2U16`
* `VAR_TEX`
* `VN_ASST2`
* `WMASK`
* `ZS_EMIT`

# Both

* `F16_TO_F32`
	* `FADD`
	* `FCMP`
* `FREXPE`
* `FREXPM`
* `FROUND`
	* `MKVEC`
* `MOV`
* `NOP`
* `QUIET`
* `S16_TO_S32`
* `S8_TO_S32`
* `SEG_ADD`
* `SEG_SUB`
* `U16_TO_U32`
* `U8_TO_U32`
* `V2F32_TO_V2F16`

# Commentary

* `FADD` schedules to both while multiplies are ordinarily only FMA. So floating-point multiplication-by-two should be lowered to `FADD x, x` before scheduling to increase flexibility, unless it could be fused into an `FMA`.

* `IADD` schedules to ADD while the generalization `IADDC` schedules to FMA. Indeed, the IR should model *only* `IADDC`, and if the second source is tied to zero, the scheduler may reinterpret as plain `IADD`. Conceptually `+IADD` is just a special case of `*IADDC`. Hence integer multiplication by two should always be lowered to `IADDC x, x, #0` freeing the scheduler.

* Likewise shift by one should be lowered to an add for scheduling. Since shifts and IMUL are both FMA, strength reduction there is irrelevant for scheduling.

* If we know _a priori_ that `A & B = 0`, then an integer add is preferred to a bitwise OR.

* `FADD` is preferred to `FMA` with multiplication-by-zero.

* `MKVEC.v2i16` with operands tied is preferred to `SWZ.v2i16`.

* `MKVEC.v4i8` is preferred to `SWZ.v4i8` _where equivalent_. Indeed the set of swizzles modeled by each are distinct, overlapping only for `b0000` and `b2222`

* `FADD.clamp_0_inf` is preferred to an open-coded `FMAX`.
