#
# Copyright (C) 2020 Collabora, Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.


from isa_parse import parse_instructions, expand_states

instructions = parse_instructions("ISA.xml")
states = expand_states(instructions)

_TABLE = [
        "+ATEST",
        "+FATAN_ASSIST",
        "+FATAN_TABLE",
        "+FCOS_TABLE",
        "+FEXP",
        "+FEXP_TABLE",
        "+FLOGD",
        "+FLOG_TABLE",
        "+FPCLASS",
        "+FPOW_SC_DET",
        "+FRCBRT_APPROX_A",
        "+FRCBRT_APPROX_B",
        "+FRCBRT_APPROX_C",
        "+FRCP",
        "+FRCP_APPROX",
        "+FRSQ",
        "+FRSQ_APPROX",
        "+FSINCOS_OFFSET",
        "+FSIN_TABLE",
    ]

_BRANCH = [
        "+BRANCH",
        "+BRANCHC",
        "+BRANCH_DIVERG",
        "+BRANCH_LOWBITS",
        "+BRANCH_NO_DIVERG",
        "+BRANCHZ",
        "+JUMP",
]

_DESCRIPTOR = [
        "+LD_CVT",
        "+LD_TILE",
        "+ST_CVT",
        "+ST_TILE",
        "+TEXC"
]

def TABLE(op):
    return (op.split(".")[0]) in _TABLE

def BRANCH(op):
    return (op.split(".")[0]) in _BRANCH

def DESCRIPTOR(op):
    return (op.split(".")[0]) in _DESCRIPTOR

def NO_TEMPS(op, start):
    return (op == '+CLPER.i32' and start == 0) or (op == '*IMULD')

# Constraint: Atomics cannot read zero (except perhaps via FAU-RAM)
# Constraint: IMULD cannot read zero (should be optimized out)
def READ_0(op):
    return not op.startswith('*ATOM_C') and not op == '*IMULD'

# Constraint: Tables cannot read their cycle's FMA
# Constraint: Branch offsets cannot read their cycle's FMA
# Constraint: Descriptors cannot read from their cycle's FMA
# Constraint: Temporaries may be clobbered across lanes
def READ_T(op, start):
    atomic = op.startswith('*ATOM_C')
    descriptor = (DESCRIPTOR(op) and start == 6) or (op == '+BLEND' and start >= 3)
    branch_offset = (op.startswith('+BRANCH') or op == '+JUMP') and start == 6
    return not (atomic or TABLE(op) or branch_offset or descriptor or NO_TEMPS(op, start))

# Constraint: Temporaries may be clobbered across lanes
def READ_T0_T1(op, start):
    return not NO_TEMPS(op, start)

for ins in states:
    is_fma = ins[0] == '*'
    srcs = states[ins][2]["srcs"]
    assert(srcs is not None)

    for start, mask in srcs:
        # Build up an accepted mask. A value is accepted if it is explicitly in
        # the mask or allowed not to be due to some constraint
        mask_ = mask

        # Constraint: The first two source of FMA cannot read from port #2
        if is_fma and start < 6:
            assert((mask & (1 << 2)) == 0)
            mask_ |= (1 << 2)

        # Constraint: Not all instructions / sources can read temporaries
        if not (READ_0(ins) if is_fma else READ_T(ins, start)):
            assert((mask & (1 << 3)) == 0)
            mask_ |= (1 << 3)

        if not READ_T0_T1(ins, start):
            assert((mask & ((1 << 6) | (1 << 7))) == 0)
            mask_ |= (1 << 6) | (1 << 7) 

        # It doesn't make sense to permute constants or uniforms, since they
        # are warp-invariant. This constraint can be introduced at a
        # higher-level.
        if ins == '+CLPER.i32' and start == 0:
            assert(mask == 0x7)
            mask_ = 0xFF

        assert(mask_ == 0xFF)

print("Success.")

