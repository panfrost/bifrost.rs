import itertools

# Model the constant word position constraints

# Table of pos bits, along with the constant index and tuple count

TABLE = [
        [0, 1],
        [0, 2],
        [0, 4],
        [1, 3],
        [1, 5],
        [2, 4],
        [0, 7],
        [1, 6],
        [3, 5],
        [1, 8],
        [2, 7],
        [3, 6],
        [3, 8],
        [4, 7],
        [5, 6]
    ]

# If 1 <= T <= 8, 0 <= C <= T, C + T <= 13, then for each 0 <= i < c, if i is
# even EXCLUSIVE-XOR T \in {3, 5, 6, 8}, then (i, t) \in P.

for (c, t) in itertools.product(range(0, 12 + 1), range(1, 8 + 1)):
    # Match on the hypotheses
    if not (c <= t and c + t <= 13):
        continue

    # Verify the conclusion
    for i in range(0, c):
        if (i % 2) != (1 if (t in [3, 5, 6, 8]) else 0):
            continue

        assert([i, t] in TABLE)

print("Success.")
