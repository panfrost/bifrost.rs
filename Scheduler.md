# Goal

The Bifrost scheduler is responsible for assigning units to instructions,
assigning slots to sources and destinations, grouping instructions into tuples,
grouping tuples into clauses and packing their constants, and grouping clauses
based on their dependencies. While doing so, it must satisfy a large number of
constraints documented elsewhere in the repository. Within the constraints, it
seeks to find an optimized schedule, minimizing register pressure (avoiding
spills and maximizing thread count) and latency (from message-passing
instructions).

# Model

The scheduler is specified for a simplified view of the linear source code of a
single basic block, outputting a simplified view of the scheduled source code.

## Input

Source code is modeled as a list of instructions, modeled as tuples $(op, defs, uses, fau, prev)$, where:

* $op \in \text{Opcodes}$ is an opaque opcode
* $defs$ is the set of values written by the instruction
* $uses$ is the list of values read by the instruction, treated as a set except where marked explicitly.
* $fau$ is the set accessed from FAU-RAM: either $32$-bit constants (satisfying $0 \leq |fau| \leq 2$ and $\forall c \in fau : 0 \leq c \leq 2^{32} - 1$), or a single opaque identifier representing a particular uniform or other preloaded constant.
* $prev$ is an instruction that must immediately precede this one in the instruction stream, or $\emptyset$ if none. Allows constructing chains for paired instructions.

We define the map $Staging$ from an instruction to the set of values used as staging registers instead of the usual register block mechanism (a subset of $defs \cup uses$). Define $Unit$ from an opcode to the set of units its last machine
instruction may run on, letting $*$ denote the FMA unit and $+$ denote the
ADD/TBL unit. We define the predicate
$Last$ specifying that an instruction must be last in the clause, and the predicate $Message$ representing a message-passing instruction. Given a source index, the $ReadT, ReadT_0T_1$ predicate indicate if the indexed source can read the respective temporary, where $ReadT$ is vacuously true for an FMA-only instruction, which uses the simplified $Read0$ to denote availability of the fast 0.

\begin{align*}
Staging & : \text{Instruction} \to \mathcal{\text{Value}} \\
Unit & : \text{Opcodes} \to \{ \{ * \}, \{ + \}, \{ *, + \} \} \\
Last & : \text{Opcodes} \to \{ T, F \} \\
Message & : \text{Opcodes} \to \{ T, F \} \\
Read0 & : \text{Opcodes} \to \{ T, F \} \\
ReadT & : \text{Opcodes} \times \{ 1, 2, 3, 4 \} \to \{ T, F \} \\
ReadT_0T_1 & : \text{Opcodes} \times \{ 1, 2, 3, 4 \} \to \{ T, F \} \\
\end{align*}

For a value in the $defs$ set of an instruction, we define the $Uses$ map to the instructions that use it:

\begin{align*}
Uses : \text{Value} & \to \mathcal{P}(\text{Instruction}) \\
x & \mapsto \{ instruction | x \in instruction.uses \}
\end{align*}

We assume instructions marked last are branches or pass messages (via staging
registers), all requiring the ADD unit.

\begin{align*}
Last(op) \lor Message(op) & \Rightarrow Unit(op) = \{ + \} \\
Staging(instruction) \ne \emptyset & \Rightarrow Message(instruction.op)
\end{align*}

## Output

The scheduler produces a list of clauses. Each clause is a list of tuples
modeled as $(*, +)$, where $*, +$ are the instructions scheduled to the FMA/ADD
units or $\emptyset$ if nothing is scheduled.

For a tuple $(I_*, I_+)$, define $Constants$ to be the constants accessed from FAU. Zero is ignored if it can be encoded for free. Formally, let $Free_*(I_*)$ be defined as

$$\begin{cases}
\{ 0 \} & \; \text{if} \; Read0(I_*) \\
\emptyset & \text{otherwise}
\end{cases}$$

Then we define

\begin{align*}
Constants : \text{Tuple} & \to \mathcal{P}(\{ 0, \dots, 2^{32} - 1 \}) \\
(I_*, I_+) & \mapsto \begin{cases}
	(I_*)_{*.fau} \setminus Free^*(I_*)) \cup (I_+)_{fau} & \text{if constants} \\
	\emptyset & \text{otherwise} \\
\end{cases}
\end{align*}

For a clause with tuples $(T_1, \dots, T_n)$, define $CConstants$ to be the number of constants required for FAU. Tuples that access two constants must access them together, but tuples that access only one can coalesce theirs together or share a constant from an earlier pair. Order very much does not matter, as the sets take care of. Also, there is no need to have a lone $0$ explicitly since we can always get $0$ from the all-zero FAU entry, saving some code size. So we define:

\begin{align*}
Pairs & = \{ Constants(T_i) \mid 1 \leq i \leq n, |Constants(T_i)| > 1 \} \\
Single & = \{ Constants(T_i) \mid 1 \leq i \leq n, Constants(T_i) \not\subset \bigcup Pairs \} \setminus \{ 0 \} \\
& \\
NConstants : \text{Clause} & \to \mathbb{N} \\
(T_1, \dots, T_n) & \mapsto |Pairs| + \lceil \frac{1}{2} |Single| \rceil
\end{align*}

# Assumptions

We assume the following properties of the linear source code:

* The contents of FAU-RAM are not permuted across lanes (via the `+CLPER.i32` instruction). With the exception of the lane ID which is not API visible, since FAU-RAM is warp-invariant, such an instruction would be an identity and can be lowered/optimized out.

* No single instruction accesses multiple 64-bit entries from FAU-RAM (e.g. uniforms and constants can't be mixed). This is impossible to schedule, so this should be lowered to moves to temporaries without any tradeoff.

* Instruction chains are valid with respect to register block requirements. In particular, the scheduler must not need to insert moves inside of a chain. <!--- TODO: does this need to be more precise? -->

* Instruction chains are no more than 4 instructions long. The starting instruction of a chain satisfies $Unit(\cdot) = \{ + \}$, succeeding instructions alternate between FMA and ADD units.

# Constraints

Scheduler constraints may be specified formally in terms of the model.

**Opcodes imply units.** Given a tuple $((op_*, \dots), (op_+, \dots))$, we require:

$$\begin{cases}* \in Unit(op_*) \\
+ \in Unit(op_+)\end{cases}$$

**Slot assignment.** Let $(T_1, \dots, T_n)$ be the tuples of a clause. Let:

\begin{align*}
T_{i.uses} & = T_{i.*.uses} \cup T_{i.+.uses} \setminus Staging(T_{i.+}) \\
T_{i.defs} & = T_{i.*.defs} \cup T_{i.+.defs} \setminus Staging(T_{i.+})
\end{align*}

First, we have:

\begin{align*}
\left| T_{1.uses} \right| & \leq 3 \\
\left| T_{n.defs} \right| & \leq 1
\end{align*}

Second, fixing each $2 \leq i \leq n$, let $T = T_i$ and $S = T_{i - 1}$, and define:

\begin{align*}
R & = \left(T_{*.uses} \cup T_{+.uses} \setminus T_{*.defs}\right) \setminus (S_{defs} \cup Staging(T_+) \\
W & = \{ def \in S_{defs} \mid Uses(def) \not\subset T\}  \setminus Staging(S_+)
\end{align*}

Then we have the constraints:

\begin{align*}
|R| & \leq 3 \\
|W| & \leq 2 \\
|R| + |W| & \leq 4 \\
R \cap W & = \emptyset \\
\end{align*}

**FAU assignment.** Given a tuple $((\dots, fau_*), (\dots, fau_+))$, we have either:

* Constants: $fau_*, fau_+ \subset \mathbb{N} \land |fau_* \cup fau_+| \leq 2$.
* Else: $(fau_* = \emptyset) \lor (fau_+ = \emptyset) \lor (fau_* = fau_+)$.

**Temporaries may not be available.** Consider successive tuples in a clause $S, T$. Let $i \in \{ 1, 2, 3, 4 \}$ be a source index, indexing into $uses$. Let $S_{defs} = S_{*.defs} \cup S_{+.defs}$. For each $instruction \in \{ T_*, T_+ \}$, we require:

$$instruction_{uses.i} \in S_{defs} \Rightarrow ReadT_0T_1(instruction_{op}, i)$$

We additionally require

$$T_{+.uses.i} \setminus Staging(T_+) \in T_{*.defs} \Rightarrow ReadT(T_{+.op}, i)$$

**Clause must not exceed 8 quadwords or tuples.** Given a clause $C$,

\begin{align*}
n & \leq 8 \\
n + NConstants(C) & \leq 13 \\
\end{align*}

**Last instructions must be last**. Given a clause with tuples $(T_1, \dots, T_n)$, we require:

$$\forall i \in \{ 1, \dots, n \} : Last(T_{i.+.op}) \Rightarrow i = n$$

**Message-passing instructions are unique in a clause.** Again given tuples $(T_1, \dots, T_n)$ we require:

$$\forall i, j \in \{ 1, \dots, n \}: Message(T_{i.+.op}) \land Message(T_{j.+.op}) \Rightarrow i = j$$

**Message-passing instructions results are delayed**. For a given set of instruction $\mathcal{I}$ in a given clause, for each $i \in \mathcal{I}$ satsifying $Message(i)$:

$$\forall j \in \mathcal{I}: i_{defs} \cap Staging(i) \cap j_{uses} = \emptyset$$

**Chained instructions are respected**. Given a list of instructions $(I_1, \dots, I_n)$ in a clause, for any $1 \leq i \leq n$

$$\begin{cases}
I_{i.prev} \ne \emptyset \Rightarrow I_{(i - 1)} = I_{i.prev} & \text{if} \; i \geq 2 \\
I_{i.prev} = \emptyset & \text{if} \; i = 1
\end{cases}$$

# Omissions

Certain hardware features are notably omitted from the model without loss of
generality.

## Scoreboarding

Given Bifrost's scheduling model is entirely based on scoreboarding, the lack
of explicit scoreboarding in the model is surprising. However, this is
justified as scoreboarding does not impose scheduling _constraints_, except for
the register access patterns already modeled. In particular, Bifrost requires that
each clause with a message-passing instruction is assigned a scoreboard slot
(generally $0$ through $5$) allowing other clauses to wait on it. However, if
multiple clauses use the same slot, the hardware will internally keep a
counter, blocking if the counter saturates, which will always be correct but
might have a small performance cost. Regardless, none of this affects the
schedule directly; the extra hardware ensures there is no need to artificially
cap the number of messages in-flight. Thus the scheduler itself may ignore the
numbering, assigning scoreboard slots after scheduling and (if desired) after
register allocation.

Dependencies between clauses can be computed directly given the model. A clause
$C$ with instructions $C_1, \dots, C_n$ depends on a clause $D$ with
instructions $D_1, \dots, D_m$ if

$$\bigcup_{i = 1}^n \left( D_{i.defs} \cap Staging(D_i) \right) \cap \bigcup_{i = 1}^m \left (C_{i.uses} \right) \ne \emptyset$$

Since this is a function of only more primitive aspects of the model, there is
no need to duplicate this information in the model and then have to maintain
this as an additional constraint.

## Practical considerations

Since staging register writes are unique in a clause (as staging registers
imply message-passing and message-passing is unique), an implementation can
implement this dependency rule by tracking the optional staging write per
clause, setting dependencies for later clauses by intersecting the uses with
the prior clauses' writes. Since correct scheduling at the clause-level will
form a topological sort, this analysis can be done in linear time walking the
program in source order.

## Staging barriers

Bifrost requires a clause to mark a "staging barrier" if it might write to a
register that was read by an outstanding message-passing instruction.

Like scoreboarding, this is a piece of metadata which can be computed without
constraining the schedule. However, it can only be computed after register
allocation, so it is strictly out of scope for the scheduler model.

After scheduling and register allocation, it may be computed by comparing the
set of registers read by pending message-passing instructions with the set of
registers written, setting the bit iff the sets intersect.

## Practical considerations

Bifrost has 64 scalar registers, so these bitsets fit snugly in a 64-bit word.

To avoid the performance cost, register allocation could extend the liveness of
input staging registers to cover until the dependency is waited on. However,
this risks harming thread count and introducing needless spilling, both of
which have higher performance costs than setting a staging barrier. Using a
round robin strategy could mitigate this.

## Flow control

Branch reconvergence is a function of the _control flow graph_, not the
schedule within a basic block. As the control flow graph is untouched by the
scheduler, reconvergence may be computed either before or after scheduling as
desired.

Write elision must be determined after scheduling but before register
allocation. Assume the last tuple of a clause performs a write (if not, write
elision is useless). Then the writing instruction $I$ within the tuple is
unique by the register block constraint. Given the set of instructions
$\mathcal{I}$ that are *not* in the first tuple of immediate successors of the
clause, write elision may be set iff

$$\{ instruction_{uses} \mid instruction \in \mathcal{I} \} \cap I_{defs} = \emptyset$$

The end-of-shader condition may be determined trivially at pack time.

<!--- TODO: Back to back conditions? -->

## Message types

Since messages are unique in a clause, so is the message type, which can be
determined by a straightforward linear walk of each clause after scheduling.

## Practical considerations

The scheduler may wish to mark the clause type when creating a clause around a
message, to avoid a separate iteration of the clause, saving a bit of
complexity and improving performance slightly. However, these steps are
logically interchangeable, hence the omission from the model.

# Equivalence

We show that a scheduled model satisfying the model constraints induces a
scheduled program satisfying the hardware constraints.

## Constrained sources

Some instructions have constraints on what sources they may read, for example forbidding temporaries. For correctly specified maps $Read0, ReadT, ReadT_0T_1$, a source $s_i$ indexed by $i \in \{ 1, 2, 3, 4 \}$ of an instruction $I$ is permitted if the following conditions holds. The equivalence of these conditions is proven in `models/src.py`, which contains concrete implementations of the maps.

## The first two sources of an FMA instruction cannot read from slot 2.

We do not model slots explicitly, rather constraining the model to ensure the induced slot assignments are valid. Thus this constraint is handled in the next section on the register block.

## Same-cycle temporaries can only be read if Read0/ReadT is true

Consider a tuple $(T_*, T_+)$.

$T_*$ requires a same-cycle temporary to include zero for free, which occurs when $0 \in (T_*)_{fau}$ and $0 \notin Constants(T_*, T_+) = ((T_*)_{fau} \setminus Free^*(T_*)) \cup (T_+)_{fau}$, implying $0 \in Free^*(T_*)$, implying $Read0(T_*)$ as required.

$T_+$ requires a same-cycle temporary for source $i$ if it uses something defined in $T_*$, that is $T_{+.uses.i} \setminus Staging(T_+) \in T_{*.defs}$. But corresponding invariant implies $ReadT(T_{+.op}, i)$, showing $ReadT(T_{+.op}, i)$ as required.

## Multi-cycle temporaries can only be read if ReadT0T1 is true

Cosinder an instruction preceded by a tuple $S$, and let $S_{defs}$ denote the values written by $S$. Then a multi-cycle temporary is needed for source $i$ iff $instruction_{uses.i} \in S_{defs} \Rightarrow ReadT_0T_1(instruction_{op}, i)$ by the corresponding constraint, as needed.

## CLPER reads only architectural registers

Since $ReadT, ReadT_0T_1$ are each false for the particular implementation of `+CLPER.i32`, no temporary can be read as seen above. Since we assume the source code does not permute the contents of FAU-RAM, by exhaustian the sources must be architectural registers.

## Register block

After register allocation is finalized, scheduled clauses induce register
blocks. In this section, we show how to pick an architecturally valid register
block given the modeled output and a register allocation given by the map

$$Reg : \text{Value} \to \{ 0, \dots, 63 \}$$

Let $S, T$ be successive tuples, wrapping around such that $S$ is the last
tuple of a clause if $T$ is the first.

Recall $|T_{uses}| \leq 3$ by the slot assignment constraints. Enumerate $T_{uses}$ as $0 \dots 2$, such that the first two sources of $T_{fma}$ if they exist are assigned to indices $0, 1$ respectively, ensuring the first two sources will not read from slot 2, and such that there are no gaps. Enumerate $S_{defs}$ as $0 \dots 1$ such that $S_{+.defs}$ is assigned to index 0 if it exists.

Suppose first $T$ is the first tuple. Leaving empty values for which the corresponding use/def does not exist, set:

* Slot 0: $\min \{ Reg(T_{uses.0}), Reg(T_{uses.1}) \}$
* Slot 1: $\max \{ Reg(T_{uses.0}), Reg(T_{uses.1}) \}$
* Slot 2: $Reg(T_{uses.2})$
* Slot 3: $Reg(S_{defs.0})$

Then all required uses/defs are read/written as required by the model, since
$|S_{defs}| \leq 1$ is constrained. Since $T_{uses}$ is a set, $T_{uses.i}$ are
disjoint since the indices $0, 1, 2$ are disjoint. Since $T_{uses.i}$ are all
simultaneously live, they all interfere, and since $R$ is a register
allocation, that implies the three are disjoint, showing the values of slots
$0, 1, 2$ are disjoint as required.

Since there are no gaps, if slot 1 is set then so is slot 0. The $\min, \max$ further ensures slot 1 is strictly greater than slot 0, noting they cannot be equal by disjointness. Thus all machine requirements are met.

Else $T$ is not the first tuple. Then defined above are sets $R, W$ of read and written values. Enumerate $R$ such that the first two sources of FMA if they exist are assigned to $R_0$ and $R_1$. Enumerate $W$ such that the def from $S_+$ is assigned to $W_0$ if it exists. Then set

* Slot 0: $\min \{ Reg(R_0), Reg(R_1) \}$
* Slot 1: $\max \{ Reg(R_0), Reg(R_1) \}$
* Slot 2: $Reg(R_2)$ if $R_2$ exists else $Reg(W_1)$
* Slot 3: $Reg(W_0)$

By a similar liveness argument, each read slot (resp. write slot, if distinct -- if not distinct to avoid clobbering must be disjoint half-register writes) is disjoint from the other read slots (resp. write slots). Since $R \cap W = \emptyset$, each $Ri, Wj$ pair is distinct, so to show $Reg(R_i), Reg(W_j)$ is distinct, it suffices to show $R_i, W_j$ are simultaneously live. Since $W_j \in W$, by definition it satisfies $Uses(W_j) \not\subset T$, so there exists some tuple besides $T$ that uses its value. But that tuple can only occur after it is written, and since $T$ is the immediate successor in the clause, it follows that some tuple later than $T$ reads $W_j$, so in particular $W_j$ is live throughout $T$. But $T$ also has $R_i$ live at the beginning to read it, so they are simultaneously live as desired.

The ordering of the slots follows as above, as do the functions by construction.

# Algorithm

In this section, we develop an algorithm to schedule linear source code (in
terms of the input model) to clause-based source code (in terms of the output
model), satisfying the necessary constraints.

We use a list scheduler, filling clauses in reverse-order one at a time with a
greedy algorith, choosing an instruction satisfying model constraints based on
a register pressure heuristic. Special priority is given to instructions that
generate values used as sources of the succeeding tuple to enable temporary
registers.

The result at each step is a valid schedule of a subset of the original
program. Since the algorithm is iterative, the proof of correctness is by
induction, where the model invariants maintained at every step correspond to
the induction hypothesis. Also, since the scheduled instruction count is an
integer, strictly increasing, and bounded above by the instruction count of the
original program (times a constant factor due to inserted moves), the algorithm
will terminate with a full program scheduled, and the final schedule will
satsify all required invariants.

<!--- TODO: Can we do better than greedy? Full optimal has far too big a search space but maybe we can compare a few different possibilities if we're not sure or something, see https://freenode.irclog.whitequark.org/panfrost/2020-11-11 -->

## Description

Construct a dependency graph of the basic block. Repeat the following until
there are no remaining instructions to be scheduled:

Assume there is an in-progress clause $\mathcal{C}$ satisfying the model
invariants. (If there is none, create an empty clause.) Let $N$ equal the number of tuples contained in $\mathcal{C}$ plus a single additional instruction at the start. If $N > 8$, then end the clause and begin a new clause, hence we assume $N + NConstants(\mathcal{C}) \leq 13$ and $N \leq 8$.

Let $m \in \{ 0, 1, 2 \}$ be the number of register writes permitted for this tuple, computed during the FMA stage of the previous tuple. Similarly let $r$ be the number of temporaries required to be loaded and $R$ be the set of possible temporary values, both from the previous tuple. If there is no previous tuple (equivalently, if scheduling the last tuple of the clause), set $m = 1$, $r = 0$, $R = \emptyset$.

We seek to schedule the next unit $U$, building the clause in reverse-order. If scheduling to FMA, let $fau = T_{+.fau}$, where $T_+$ is the instruction in the same tuple scheduled to ADD, otherwise let $fau = \emptyset$.

Consider the set of instructions that are possible to schedule, that is, sink
nodes on the dependency graph. Consider the subset of instructions $I$ satisfying:

* Units match: $U \subset Unit(I)$
* TODO: Slots
* If $fau \ne \emptyset$, then $I_{fau} = \emptyset$ or $I_{fau} = fau$ or if $fau \subset \mathbb{N}$, then $|fau \cup I_{fau}| \leq 2$.
* Letting $\mathcal{C}'$ denote $\mathcal{C}$ with $I$ added: $N + NConstants(C) \leq 13$
* If $\mathcal{C}$ is nonempty, then $\not Last(I)$
* If $\mathcal{C}$ contains a message-passing instruction, then $\not Message(I)$.
* If $Message(I)$, then $I_{defs} \cap Staging(I) \cap \cup_{J \in \mathcal{C}} J_uses = \emptyset$

If the subset is empty, schedule a no-op instruction and proceed.

Otherwise, choose an instruction $I$ from that subset using some heuristic.

If scheduling FMA, compute $m$ for the next scheduled tuple as:

<!--- TODO compute m based on uses and forced temporaries -->
<!--- TODO compute r based on uses and forced temporaries -->
<!--- TODO compute R based on uses and forced temporaries -->

Schedule it to active clause. If $I.prev$ is nonempty, schedule it as well, and
similarly for $I.prev.prev, I.prev.prev.prev$.

## Proof of correctness

First, note an empty clause satisifes all model constraints vacuously.

Otherwise, suppose the existing clause $\mathcal{C}$ satisfies the invariants. Consider the new clause $\mathcal{C}'$ obtained by scheduling the new instruction to the next available unit.

The only change to units is the addition of $I$ to $\mathcal{C'}$, but $U \subset Unit(I)$ by construction, and since the unit constraint is satisfied for $\mathcal{C}$, it is for $\mathcal{C}'$ as well.

TODO: SLOT

For the FAU constraint, it suffices to show only the modified tuple meets it. If $fau = \emptyset$ in the original tuple, then the FAU assignment constraint is already met. Else by construction the new tuple has $fau_* = I_fau, fau_+ = fau$, so by construction the constraint is met.

TODO: Temporaries

By construction, $N \leq 8$, $N + NConstants(\mathcal{C}') \leq 13$ are satisfied.

The original clause $\mathcal{C} = (T_1, \leq, T_n)$ satisfies the last instruction criterion:

$$\forall i \in \{ 1, \dots, n \} : Last(T_{i.+.op}) \Rightarrow i = n$$

By adding one instruction we yield a reindexed $\mathcal{C}' = (T_0, T_1, \leq, T_n)$. For any $i \in \{ 1, \dots, n \}$, the above fact for $Last$ holds. So it suffices to consider $i = 0$. If $\mathcal{C}$ is empty, $n = 0 \Rightarrow i = n$ so it holds. Otherwise if $\mathcal{C}$ is nonempty, by construction $Last(I)$ does not hold so it is vacuously true. So the invariant holds for $\mathcal{C}'$.

Similar reasoning shows message passing uniqueness is upheld.

Suppose $Message(I)$. Let $J \in \mathcal{C}'$. By construction

$$I_{defs} \cap Staging(I) \cap \cup_{j \in \mathcal{C}} J_uses = \emptyset$$

So in particular since $J \in \mathcal{C} \Rightarrow J \in \mathcal{C}'$:

$$I_{defs} \cap Staging(I) \cap J_uses = \emptyset$$

So the message-passing result requirement is met.

Finally, since chains are respected on $\mathcal{C}$, and the entire chain is added to $\mathcal{C}'$, chains are respected in $\mathcal{C}'$. <!--- TODO this is vague and chains are wrong in other places -->

## Heuristic

As seen, the algorithm correctness does not depend on the instruction selection heuristic.

<!-- TODO: pick some heuristic? latency, register pressure considerations. Irrelevant for correctness, of course. If opening a clause and want a last instruction / message passing instruction, have little choice in the matter to avoid a silly tiny clause. -->

## Practical considerations

The dependency graph should be computed at 16-bit granularity, since that is
the minimum size the GPU can write independently, so there is no loss of
generality instead of 8-bit granularity. But computing at 32-bit granularity
would overly constrain certain 16-bit code sequences.

Dependency graph computation is $O(N)$ to the size of the program. It suffices
to consider only the _last_ instruction to depend on for a given type of
dependency (read-after-write, write-after-read, write-after-write). By doing
so, the degree of all vertices on the dependency graph is bounded by the sum of
the maximum number of sources and twice the maximum number of destinations,
scaled by $\frac{32}{16} = 2$ due to $16$-bit granularity. Then for
sufficiently large programs, the dependency graph is sparse and admits an
efficient adjacency list representation, with $O(N)$ space requirements and
$O(1)$ search.

The set of instructions with no outstanding dependents may be maintained as a
worklist, initialized in a single $O(N)$ pass over the vertices of the graph
and updated in $O(1)$ time each step with the bounded adjacency list
representation, for $O(N)$ total time to maintain.

### Illustrations

Consider the same program and (complete) dependency graph. The only available
isntruction is the STORE, which we schedule.

```
{
	---
	+STORE.i32 U2, @4
}
```

Pop the store off the dependency graph, since it has been scheduled now.

![Dependency graph with STORE popped off](Diagrams/DAG-2.png){width=30%}

We would like to fill the FMA slot. So consider the set of schedulable
instructions from the dependency graph. In this case, only `XOR`. We check the
constraints:

* `XOR.i32` runs on FMA.
* The store reads $4$. Adding on the XOR will write $4$ and read $3$ and $1$, so the store's read will cancel out to a $T$. Since the XOR is not used anywhere, no pending writes are created. So the clause will just require slots to read $3$ and $1$, with both write slots free, which works.
* The store requires `U2` in FAU. The XOR has no requirements from FAU, so this is vacuously satisfied.
* Adding the XOR would mean the store reads from the same cycle. Uh-oh -- that requires $ReadT$, which `STORE` doesn't have. But it's used as a staging register, so it's fine.
* We have not added any constants or tuples, so the size is still okay.
* We're inserting in reverse, so the last invariant is not changed.
* `XOR` is not a message-passing instruction, so that invariant is unchanged.
* We' occur before the STORE, which doesn't even write anything, so there's no dependency issue.
* No instruction chain, so that's fine.

After verifying these constraints, we see it works! So we can add it in, to
produce a new clause that still satisfies our invariants, and pop it off the dependency graph.

```
{
	*XOR.i32 4, 3, 1
	+STORE.i32 U2, 4
}
```

![Dependency graph with XOR popped off](Diagrams/DAG-3.png){width=30%}

Then the whole cycle repeats! We can't take message passing instructions, so our only option is scheduling the multiply. But that can't go on ADD, so we're forced to insert a no-operation to the ADD slot.

```
{
	---
	+NOP.i32
	*XOR.i32 4, 3, 1
	+STORE.i32 U2, 4
}
```

As for the multiply on the FMA slot:

* There's no store required in the last op, and we'll need at most 2 read slots, so we're ok there. No FAU either.
* This will be fed into the XOR via a temporary, but XOR can read temporaries, so that's fine.
* No new constants. We previously had $n = 1, NConstants(C) = 0$, adding a new tuple bumps that to $n = 2, NConstants(C) = 0$ whose sum is still less than $13$, and $n \leq 8$ still holds.
* No change to last, message-passing, or chains, so that's all vacuously still true.

So we can schedule and pop it off.

```
{
	*MUL.i32 3, 2, R1
	+NOP.i32
	*XOR.i32 4, 3, 1
	+STORE.i32 U2, 4
}
```

The same reasoning applies to the add, but then we're stuck, since our only remaining options are message-passing instructions. So we are forced to fill with a NOP, and close the clause.

```
{
	*NOP.i32
	+ADD.i32 2, 0, R0
	*MUL.i32 3, 2, R1
	+NOP.i32
	*XOR.i32 4, 3, 1
	+STORE.i32 U2, 4
}
```


